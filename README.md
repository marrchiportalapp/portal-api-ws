# Employee Portal Webservice
This layer provides the webservices to be consumed by clients

## Dependencies
- Java8 jdk
- Maven
- Eclipse IDE (or any Java IDE)
- This project is dependent on the api-modules project. Please follow instructions on how to build [here](https://bitbucket.org/marrchiportalapp/api-modules).

## Getting Started

```bash
git clone https://<your account>@bitbucket.org/marrchiportalapp/portal-api-ws.git
cd portal-api-ws
mvn install
mvn spring-boot:run
```

- Navigate your browser to the webservice documentation [here](http://localhost:9080/jsondoc-ui.html)

- Enter http://localhost:9080/jsondoc for the jsondoc url.

##Building
If you prefer to create a war file for deployment, simply run the following commands below to generate a war file.