
package com.marrchi.api.repository;

import com.api.data.core.repository.BaseCrudRepository;
import com.marrchi.api.model.User;


public interface UserRepository extends BaseCrudRepository<User, String> {
	 
	/**
	 * Search for user by username.
	 * @param username
	 * @return
	 */
    public User findByUsernameIgnoreCase(String username);

}
