package com.marrchi.api;

import org.jsondoc.spring.boot.starter.EnableJSONDoc;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJSONDoc
@EnableAutoConfiguration
@EntityScan(basePackages={"com.marrchi.api.model"})
@EnableJpaRepositories(basePackages="com.marrchi.api.repository")
@ComponentScan(basePackages={"com.api", "com.marrchi.api"})
public class Application {

    public static void main(String[] args) throws Throwable {
        SpringApplication.run(Application.class, args);
    }
}