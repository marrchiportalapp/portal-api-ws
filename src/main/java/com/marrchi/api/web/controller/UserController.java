/**
 * 
 */
package com.marrchi.api.web.controller;

import java.util.List;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.jsondoc.core.annotation.ApiPathParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.data.core.service.Service;
import com.api.restdata.controller.BaseController;
import com.marrchi.api.model.User;
import com.marrchi.api.service.UserService;
import com.marrchi.api.web.controller.dto.UserDto;

/**
 * @author cmariano
 *
 */

@RestController
@Api(name = "Users", description = "API webservice for users")
public class UserController extends BaseController<User, String, UserDto> {
	
	private static final String PARTIAL_URL = "users";
	
	@Autowired
	private UserService userService;

	@Override
	protected Service<User, String> getService() { 
		return userService;
	}
	
	/*
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 * Common methods for controllers. Please use for reference.
	 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
	 */

	@RequestMapping(value="/"+ PARTIAL_URL +"/{id}", method = RequestMethod.GET)
	@ApiMethod(path = "/"+ PARTIAL_URL +"/{id}", description = "Fetch a resource by id", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	protected User getResourceById(@PathVariable(value="id", required=false) @ApiPathParam(name = "id", description = "This resource ID") String id) {
		return super.getResourceById(id);
	}
	
	@RequestMapping(value="/" + PARTIAL_URL, method = RequestMethod.GET)
	@ApiMethod(path = "/" + PARTIAL_URL, description = "Fetch all resources", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	protected List<User> getResourceAll() { 
		return super.getResourceAll();
	}
	
	@RequestMapping(value="/" + PARTIAL_URL, method = RequestMethod.POST)
	@ApiMethod(path = "/" + PARTIAL_URL, description = "Adds or updates a resource", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	protected User addOrUpdateResource(@Validated @RequestBody User domain) {
		return super.addOrUpdateResource(domain);
	}
	
	@RequestMapping(value="/" + PARTIAL_URL + "/{id}", method = RequestMethod.DELETE)
	@ApiMethod(path = "/" + PARTIAL_URL + "/{id}", description = "Deletes a resource given the id", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	protected void deleteResource(@PathVariable(value="id", required=false) @ApiPathParam(name = "id", description = "This resource ID") String id) {
		super.deleteResource(id);
	}
}
