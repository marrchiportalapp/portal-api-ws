/**
 * 
 */
package com.marrchi.api.web.controller.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.jsondoc.core.annotation.ApiObject;
import org.jsondoc.core.annotation.ApiObjectField;

/**
 * Users of the application.
 * @author cmariano
 *
 */
@ApiObject(name="Users", description="Represents the users that could use this application")
public class UserDto {

	@NotNull(message="error.field.notnull")
	@Size(message="error.field.notempty", min=1)
	@ApiObjectField
	private String username;
	
	@NotNull(message="error.field.notnull")
	@Size(message="error.field.notempty", min=1)
	@ApiObjectField
	private String password;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
