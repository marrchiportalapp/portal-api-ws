/**
 * 
 */
package com.marrchi.api.web.controller;

import org.jsondoc.core.annotation.Api;
import org.jsondoc.core.annotation.ApiMethod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.api.data.core.service.Service;
import com.api.restdata.controller.BaseController;
import com.marrchi.api.model.User;
import com.marrchi.api.service.UserService;
import com.marrchi.api.web.controller.dto.UserDto;

/**
 * @author cmariano
 *
 */

@RestController
@Api(name = "Signup", description = "API webservice for signing up users")
public class SignupController extends BaseController<User, String, UserDto> {
	
	@Autowired
	private UserService userService;

	@Override
	protected Service<User, String> getService() { 
		return userService;
	}
	

	@RequestMapping(value="/signup", method = RequestMethod.POST, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes={MediaType.APPLICATION_JSON_VALUE})
	@ApiMethod(path = "/signup", description = "Signup a new user", produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = { MediaType.APPLICATION_JSON_VALUE })
	protected User getResourceById(@Validated @RequestBody UserDto userDto) {
		User newuser = convertToEntity(userDto, User.class);
		return getService().createOrUpdate(newuser);
	}
}
