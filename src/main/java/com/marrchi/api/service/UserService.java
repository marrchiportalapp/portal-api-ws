package com.marrchi.api.service;

import com.api.data.core.service.Service;
import com.marrchi.api.model.User;

/**
 * Service classes for Users
 * @author cmariano
 *
 */
public interface UserService extends Service<User, String> {

}
