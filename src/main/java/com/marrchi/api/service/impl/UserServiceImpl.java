package com.marrchi.api.service.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.api.data.core.repository.BaseCrudRepository;
import com.api.data.core.service.AbstractService;
import com.marrchi.api.model.User;
import com.marrchi.api.repository.UserRepository;
import com.marrchi.api.service.UserService;

/**
 * Service classes for Users
 * @author cmariano
 *
 */
@Component
@Transactional
public class UserServiceImpl extends AbstractService<User, String> implements UserService, UserDetailsService {

	@Autowired
	private UserRepository userRepository;
	
	@Override
	public BaseCrudRepository<User, String> getRepository() { 
		return userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userRepository.findByUsernameIgnoreCase(username);
		if (user != null) {
			return new org.springframework.security.core.userdetails.User(
					user.getUsername(), user.getPassword(), 
					getAuthorities(user));
		}
		throw new UsernameNotFoundException(String.format("[%s] not found", username));
	}
	
	private Collection<GrantedAuthority> getAuthorities(User user){
		List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
		authList.add(new SimpleGrantedAuthority("ROLE_USER"));
		return authList;
    }

}
